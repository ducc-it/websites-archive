# Archivio dei siti web delle edizioni passate

Questo repository contiene un archivio dei siti web delle edizioni 2013, 2014 e
2015 della DUCC-IT. Una versione live è visualizzabile su:

* [2013.ducc.it](https://2013.ducc.it)
* [2014.ducc.it](https://2013.ducc.it)
* [2015.ducc.it](https://2013.ducc.it)

L'archivio è stato realizzato con un `wget -r` dato che il sorgente e il
database sono stati persi.
